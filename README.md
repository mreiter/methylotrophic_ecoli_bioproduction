# A synthetically methylotrophic Escherichia coli as a chassis for bioproduction from methanol
Data and code repository for Figures and Supplementary Data.


To facilitate running the python code, create a conda environment (python version `3.12.0`) from the `conda_environment.yml` file.

Supplementary Data Tables 4 and 5 are part of Extended Data Figure 3b. Supplementary Data Table 7 is part of Extended Data Figure 4b.
